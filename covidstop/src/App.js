import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {useState ,useEffect } from 'react';
import CommodityRecord from "./CommodityRecord";
import MemberRecord from "./MemberRecord";
function App() {

	return(

		<Router >
			<Switch>
				<Route path="/member">
					<MemberRecord />
				</Route>
				<Route path="/">
					<CommodityRecord />
				</Route>
		</Switch>
		</Router>
	)

}


export default App;

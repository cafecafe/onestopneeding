
import {exportShipmentOrder } from './ExcelSave';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import {InputGroup, FormControl,Button,Table, ListGroup,Badge,Navbar,Container,Modal} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {useState ,useEffect } from 'react';
import ExcelLoad from './ExcelLoad';
import {config} from "./config";
import { useHistory} from "react-router-dom";
let key_num = 0;
function TableList(props) {
  const [tag, setTag] = useState("");
  const [value, setValue] = useState("$");
  const [key ,setKey] = useState(0);
  const renderOrderOnTable = () => {
    if(typeof(props.state) != 'object') return;
    return props.state.map((element,index) => <tr align="center" key={props.title+key}> 
      <td>{element.tag}</td> 
      <td>{element.value}</td>
      <td><Button variant="danger"　onClick={()=>removeOrder(index,props)}>-</Button></td>  </tr> )
  }
  const removeOrder = (index,props) => {
    console.log(props);
    let right_arr = props.state.slice(index+1, props.state.length);
    let left_arr = props.state.slice(0);
    left_arr.length = index ;
    console.log(props.state);
    props.setState(left_arr.concat(right_arr));
  }
  const pushToOrder = (setState, state= []) => {
    if(value.length == 0) {
      alert("請輸入值");
      return ;
    }
    setTag("");
    setState(state.concat({id:null ,tag : tag, value : value}));
    if(props.valuetitle == "金額") setValue("$")
    else setValue("");
  } 
  useEffect(() => {
    if(props.valuetitle == "金額") setValue("$")
          else setValue("");
  },[])
  let inputType;
  if(props.valuetitle.indexOf("數量") >0) {
     inputType = "number"
  }
  else inputType = "text";
  return (
    <>
      <style type="text/css">
        {`
        .btn-flat {
          background-color: purple;
          color: white;
        }

        .btn-xxl {
          padding: 1rem 1.5rem;
          font-size: 1.5rem;
        }
        `}
      </style>
      <Row>
        <Col sm={1} >
          <p  align="center" sytle={{fontSize :"33px !important"}}>
            {props.title}  &nbsp;   
            <Button onClick={() => pushToOrder(props.setState , props.state)}>+</Button>
          </p>
        </Col>
        <Col sm={9}>
          <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
            <Col sm={5}>
            <Row>
              <InputGroup size="mb-3" className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="inputGroup-sizing-sm">{props.tagtitle}</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl  aria-label="Small" aria-describedby="inputGroup-sizing-sm"onChange ={(event) => setTag(event.target.value)} value={tag}/>
              </InputGroup>   
            </Row>
            <Row>
                  <Form.Text  muted> 
                    {props.sign}
                  </Form.Text>
            </Row>

            </Col>
            <Col sm={5}>
              <InputGroup size="mb-3" className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="inputGroup-sizing-sm" >{props.valuetitle}</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl type={inputType} aria-label="Small" aria-describedby="inputGroup-sizing-sm" onChange ={(event) => setValue(event.target.value)} value={value}/>
              </InputGroup>

            </Col>
            <Col sm={2}> </Col>
          </Form.Group>
        </Col>
      </Row>
      <Col align="center">
        <Table striped bordered hover >
          <thead align="center">
            <tr>
              <th>{props.tagtitle}</th>
              <th>{props.valuetitle}</th>
              <th>刪除</th>
            </tr>
          </thead>
          <tbody>
            {renderOrderOnTable()}
          </tbody>
        </Table>
      </Col>
      <hr style={ {border : '1px solid #adb5bd'}}></hr>

    </>
  )
}


function CommodityRecord() {
  let history  = useHistory();
  const [searchStr, setsearchStr ] = useState( "");
  const [searchStartDate, setsearchStartDate] = useState("");
  const [searchEndDate, setsearchEndDate] = useState("");
  const [commodities, setCommodities] = useState([]);
  const [commodityid, setcommodityid] = useState(-1);
  const [serial, setserial ] = useState("");
  const [name, setname ] = useState("");
  const [amount, setamount ] = useState("");
  const [billtime, setbilltime ] = useState( "");
  const [sentence, setsentence ] = useState( "");
  const [price, setprice ] = useState( "");
  const [size, setsize ] = useState( "");
  const [color, setcolor ] = useState( "");
  const [style, setstyle ] = useState( "");
  const [example, setexample ] = useState( "");
  const [remark, setremark ] = useState( "");
  const [saveAmount, setsaveAmount ] = useState("");
  const [wholesaleOrder, setwholesale ] = useState([]);
  const [membersaleOrder, setmembersale ] = useState([]);
  const [memberOrder, setmemberOrder ] = useState([]);
  const [clientOrder, setclientOrder ] = useState([]);
  const [manuOrder, setmanuOrder ] = useState([]);
  const [CommodityListShow, setCommodityListShow] = useState(false);
  const [MemberListShow, setMemberListShow] = useState(false);
  const [articleShow ,setArticleShow] = useState(false);
  const [article ,setArticle] = useState(false);
  const [sort , setSort] = useState(false);
  const removeCommodity = (index,props) => {
    console.log(props);
    let right_arr = props.state.slice(index+1, props.state.length);
    let left_arr = props.state.slice(0);
    left_arr.length = index ;
    console.log(props.state);
    props.setState(left_arr.concat(right_arr));
    fetch(config.api.url+'/commodities/'+props.state[index].id, {
        method: 'DELETE',
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    }).then(res => {
        if(res.status !=200) throw res.status; 
        return res.json()})
    .then(data => console.log(data))
    .catch(e => {
      alert("刪除失敗");
    })


  }
  const pullCommodity = (obj, index = commodities.length) => {
    console.log(obj);
    setserial(obj.serial || "");
    setname(obj.name || "");
    setamount(obj.amount || "");
    setbilltime(obj.bill_time || "");
    setsentence(obj.sentence || "");
    setprice(obj.price || "");
    setsize(obj.size || "");
    setcolor(obj.color || "");
    setstyle(obj.style || "");
    setexample(obj.example || "");
    setremark(obj.remark || "");
    setsaveAmount(obj.saveAmount || "");
    setwholesale(obj.whole_sale  || []);
    setmembersale(obj.member_sale  || []);
    setmemberOrder(obj.member_order  || []);
    setclientOrder(obj.client_order  || []);
    setmanuOrder(obj.manu_order  || []);
    setcommodityid(index);
     setCommodityListShow(false);
  }
  const pushCommodity = () => {
     const obj = {
      serial : serial ,
      name : name ,
      amount : amount ,
      bill_time : billtime ,
      sentence : sentence ,
      price : price ,
      size : size ,
      color : color ,
      style : style ,
      example : example ,
      remark : remark ,
      saveAmount : saveAmount ,
      whole_sale : wholesaleOrder,
      member_sale : membersaleOrder,
      member_order : memberOrder,
      client_order : clientOrder,
      manu_order : manuOrder,
     }
     obj['bill_time'] = new Date(billtime).toUTCString();
     console.log(commodityid);
    // if(concat){
    //   setCommodities(commodities.concat(obj));
    //   pullCommodity({});
    // }
    // else {
    //     let temp_arr = commodities.slice(0);
    //     temp_arr[commodityid] =obj;
    //     setCommodities(temp_arr);
    // }

    fetch(config.api.url+'/commodities', {
        method: 'POST',
        body: JSON.stringify(obj),
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    }).then(res => {
        if(res.status !=200) throw res.status; 
        return res.json()})
    .then(data => {
      setCommodities(preCommodities => {
        let nextCommodities = preCommodities.slice(0);
        if(data.updated){
          nextCommodities[commodityid] =data.data;
        }
        else nextCommodities[commodities.length] =data.data;
        return nextCommodities;

      });
      console.log(data.data);
      alert("儲存成功");
    })
    .catch(error => alert('儲存失敗'))
    setcommodityid(commodityid);
    setArticleShow(true)
  }


  useEffect(() =>  
    fetch(config.api.url+'/commodities', {
        method: 'GET',
        headers: new Headers({
          'Content-Type': 'application/json'
        })
    })
    .then(res =>{
      if(res.status !=200) throw "儲存失敗"; 
      return res.json()
    })
    .then(data => {
        setCommodities(data)} )
    ,[]);
  const handleSaveAmount = (member_id, commodity_id, commodity_index, outAmount) => {
    fetch(config.api.url+'/commodities/'+commodity_id, {
        method : "PUT",
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body : JSON.stringify({variation : outAmount, member_id : member_id})
    })
    .then( res => {
        if(res.status != 200) {
          alert("錯誤");
          throw "";
        }
      return res.json()
    })
    .then( json => {
        console.log("PUT:", json);
        if(json.description != 'OK'){
         alert("庫存不足");
         return ;
       }

        alert("出貨成功")
        setCommodities((preCommodities) => {
          let nextCommodities = preCommodities.slice(0);
          setsaveAmount(json.data.saveAmount);
          nextCommodities[commodity_index] = json.data;
          nextCommodities[commodity_index]["whole_sale"] = preCommodities[commodity_index].whole_sale;
          nextCommodities[commodity_index]["member_sale"] = preCommodities[commodity_index].member_sale;
          nextCommodities[commodity_index]["manu_order"] = preCommodities[commodity_index].manu_order;
          nextCommodities[commodity_index]["client_order"] = preCommodities[commodity_index].client_order;
          return nextCommodities;

        })

    })
  }
  const compareSerial = (a,b) => {
    const handleSerial = (serial) => {
      if(!serial) return "" 
      const year = serial.slice(3,5);
      const month = serial.slice(1,3);
      const date = serial.slice(8);
      const no = serial.slice(5,8);
      return year+month+date+no;
    }
    const handle_a = handleSerial(a.serial);
    const  handle_b = handleSerial(b.serial);

    if(handle_a > handle_b) return 1;
    else if(handle_a < handle_b) return -1;
    return 0;
  }

  const compareName= (a, b) => {
    if(a.name > b.name) return 1;
    else if(a.name < b.name) return -1;
    return 0;
  }

  const compareSize = (a, b) => {
    if(a.size > b.size) return 1;
    else if(a.size < b.size) return -1;
    return 0;
  }
  const compareColor = (a,b) =>{
    if(a.color > b.color) return 1;
    else if(a.color < b.color) return -1;
    return 0;

  }
  const compareBillDate = (a,b) => {
    return new Date(a.bill_time) - new Date(b.bill_time)
  }
  const compareRecordDate = (a,b) => {
    return new Date(a.update_at) - new Date(b.update_at)
  }
  const handleSort = (compare)  => {
    setSort(!sort);
    if(sort) {

      setCommodities(commodities.slice(0).sort(compare));
    }
    else setCommodities(commodities.slice(0).sort(compare).reverse());
  }

  return (
    <div>
<Navbar expand="lg" variant="light" bg="light" sticky="top" as={Row} >
<Col md={1}></Col>
<Col md={4} > <Badge variant="secondary">
  <h1>商品資料紀錄表</h1> </Badge>  </Col>
  <Col md={1}> <Button variant="primary"　onClick={() => pullCommodity({}) }>新增</Button></Col>
                         <Col md={1}> <Button variant="success"　onClick={() => pushCommodity() }>儲存</Button></Col>
                      {/*<Col md={1}> <Button  variant="primary" onClick={()=> handleExportAll(commodities)}>匯出po文檔</Button></Col>*/}
  <Col md={1} align="center">
          <Navbar.Brand >
               <Button onClick={() => {setCommodityListShow(true);
                setsearchStr(""); 
                setsearchEndDate(""); 
                setsearchStartDate("")}
              }>
              商品清單
            </Button> 
          </Navbar.Brand>
        </Col>
  <Col md={1} align="center">
    
              <Navbar.Brand >
               <Button onClick={() => {setMemberListShow(true); 
                setsearchStr(""); 
                setsearchEndDate(""); 
                setsearchStartDate("")}
              }>
              會員訂購清單
            </Button> 
          </Navbar.Brand>

  </Col>
  <Col md={1} align="center">
    
              <Navbar.Brand >
               <Button onClick={() => history.push("/member")}>
                會員資料新增
            </Button> 
          </Navbar.Brand>
        
  </Col>
  <Col md={1} align="center">
    
              <Navbar.Brand >
               <Button onClick={() => exportShipmentOrder(commodities)}>
                會員訂單匯出
            </Button> 
          </Navbar.Brand>
        
  </Col>
</Navbar>
      <Row>
        <Col >
          <Form>
            <Form.Row>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >產品編號</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                    onChange = {(event)=> setserial(event.target.value)}
                    value={serial}/>
                </InputGroup>
              </Col>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >產品名稱</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setname(event.target.value)} value={name}/>
                </InputGroup>
              </Col>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >限量</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setamount(event.target.value)}
                  value={amount}/>
                </InputGroup>
              </Col>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >上架日期</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  type="date" 
                  onChange = {(event)=> {setbilltime(event.target.value) ;console.log(event.target.value)}}
                  value={billtime}/>
                </InputGroup>
              </Col>
            </Form.Row>
            <br></br>
            <Form.Row>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >售價</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setprice(event.target.value)} value={price}/>
                </InputGroup>
              </Col>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >尺寸</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setsize(event.target.value)} value={size}/>
                </InputGroup>
              </Col>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >顏色</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event)=> setcolor(event.target.value)} value={color}/>
                </InputGroup>
              </Col>
              <Col md="3">
                <InputGroup size="sm">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-lg" >進貨</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
                  onChange = {(event) => setsaveAmount(event.target.value)} value={saveAmount}/>
                </InputGroup>
              </Col>
            </Form.Row>
                        <br></br>
        <Form.Row >
          <Col md='6'>
            <InputGroup>
              <InputGroup.Text>說明</InputGroup.Text>
              <FormControl  rows="8" as="textarea" aria-label="With textarea"  onChange={(e)=> setsentence(e.target.value)} value={sentence}/>
           </InputGroup>
          </Col>
          <Col md='6'>
            <InputGroup  as={Col}>
              <InputGroup.Text>尺寸/顏色</InputGroup.Text>
              <FormControl as="textarea" aria-label="With textarea" value={saveAmount} onChange={(e)=> setexample(e.target.value)} value={example}/>
           </InputGroup>
           <br></br>
            <InputGroup as={Col}>
              <InputGroup.Text>留言範例</InputGroup.Text>
              <FormControl as="textarea" aria-label="With textarea" value={saveAmount} onChange={(e)=> setremark(e.target.value)}value={remark}/>
           </InputGroup>
           <br></br>

          </Col>
        </Form.Row>
      <hr style={ {border : '1px solid #adb5bd'}}></hr>
           <TableList sign="超過1以上再輸入，範例 : 2個-、3盒-。" title="批發"　tagtitle= "數量" valuetitle="金額" setState={setwholesale} state={wholesaleOrder}/> 
           <TableList title="團購"　sign="超過1以上再輸入，範例 : 2個-、3盒-。" tagtitle="數量" valuetitle="金額" setState={setmembersale} state={membersaleOrder}/>  
           <TableList title="會員下單" tagtitle="編號" valuetitle="訂購數量"　setState={setmemberOrder} state={memberOrder}/>  
           <TableList title="客戶下單"　tagtitle="日期" valuetitle="訂購數量" setState={setclientOrder} state={clientOrder}/> 
           <TableList title="廠商下單"　tagtitle="日期" valuetitle="訂購數量" setState={setmanuOrder} state={manuOrder}/>   
        </Form>
      </Col>
    </Row>
      <Modal
        size="lg"
        show={CommodityListShow}
        onHide={() => setCommodityListShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
        align="center"
      >  
            <h1>商品清單</h1>     

        <Modal.Body>
            <ExcelLoad  setState={setCommodities} /> 
                        <hr style={ {border : '1px solid #adb5bd'}}></hr>
            <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-lg" >搜尋商品</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
            onChange = {(event)=> setsearchStr(event.target.value)} value={searchStr}/>
            </InputGroup>
            <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-lg" >開始日期</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
            onChange = {(event)=> setsearchStartDate(event.target.value)} value={searchStartDate} type="date"/>
            <Button size="sm" variant="secondary" id="button-addon2" onClick={()=>setsearchStartDate("")}>
              清空
            </Button>
            </InputGroup>
            <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-lg" >結束日期</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
            onChange = {(event)=> setsearchEndDate(event.target.value)} value={searchEndDate} type="date"/>
            <Button size="sm" variant="secondary" id="button-addon2" onClick={()=>setsearchEndDate("")}>
              清空
            </Button>
            </InputGroup>
          <Table striped bordered hover size="sm">
            <thead >
              <tr align="center">
                <th onClick={() =>handleSort(compareSerial)}>產品編號</th>
                <th onClick={() => handleSort(compareName)}>產品名稱</th>
                <th onClick={()=> handleSort(compareSize)}>尺寸</th>
                <th onClick={()=> handleSort(compareColor)}>顏色</th>
                <th onClick={()=> handleSort(compareBillDate)}>上架日期</th>
                <th>庫存</th>
                <th> 功能</th>
              </tr>
            </thead>
            <tbody>
              {
 
                commodities.map((element,index) => {
                    let str = (element.serial ?? "")
                      + (element.name ?? "")
                      + (element.size ?? "")
                      + (element.color ?? "");
                    const isInTime = searchStartDate =="" || searchEndDate == "" ||
                        (new Date(element.bill_time) >= new Date(searchStartDate)  &&
                        new Date(element.bill_time) < new Date(searchEndDate) );
                    const hasStr = searchStr == "" || str.includes(searchStr)  ;
                    if(
                        hasStr  &&   isInTime  && element.name             
                      ) {
                          let date =new Date(element.bill_time);
                          let utc_date = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
                      return (
                        <tr key={index}>
                          <td>{element.serial} </td>
                          <td>{ element.name}</td>
                          <td>{element.size}</td>
                          <td>{element.color}</td>
                          <td>{new Date(utc_date).toLocaleDateString()}</td>
                          <td>{element.saveAmount}</td>
                          <td>
                            <Button size="sm" variant="primary" onClick={() => pullCommodity(commodities[index],index)}>選</Button>
                            <Button size="sm" variant="danger" onClick={() => removeCommodity(index, {state: commodities, setState: setCommodities})}>刪</Button>   

                          </td>
                        </tr>
                      )
                    }

                })
              }
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
      <Modal
        size="lg"
        show={MemberListShow}
        onHide={() => setMemberListShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
        align="center"
      >  
            <h1>會員清單</h1>     

        <Modal.Body>
                        <hr style={ {border : '1px solid #adb5bd'}}></hr>
            <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-lg" >搜尋</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
            onChange = {(event)=> setsearchStr(event.target.value)} value={searchStr}/>
            </InputGroup>
            <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-lg" >開始日期</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
            onChange = {(event)=> setsearchStartDate(event.target.value)} value={searchStartDate} type="date"/>
            <Button size="sm" variant="secondary" id="button-addon2" onClick={()=>setsearchEndDate("")}>
              清空
            </Button>
            </InputGroup>
            <InputGroup size="sm">
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroup-sizing-lg" >結束日期</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" 
            onChange = {(event)=> setsearchEndDate(event.target.value)} value={searchEndDate} type="date"/>
            <Button size="sm" variant="secondary" id="button-addon2" onClick={()=>setsearchEndDate("")}>
              清空
            </Button>
            </InputGroup>
          <Table striped bordered hover>
            <thead>
              <tr align="center">
                <th>會員編號</th>
                <th onClick={()=> handleSort(compareSerial)}>產品編號</th>
                <th onClick={()=> handleSort(compareName)}>產品名稱</th>
                <th onClick={()=> handleSort(compareRecordDate)}>紀錄時間</th>
                <th onClick={()=> handleSort(compareSize)}>尺寸</th>
                <th onClick={()=> handleSort(compareColor)}>顏色</th>
                <th>訂購數量</th>
                <th>出貨</th>
                <th>出貨時間</th>
              </tr>
            </thead>
            <tbody>
              {
 
               commodities.map((element,index) => {
                  return element.member_order.reduce((collector, order) =>{
                      if(order.shipment) order.shipment_date = new Date(element.updated_at).toLocaleString(); 
                      else order.shipment_date = "";
                      let str = (element.serial ??"" )
                        + (element.name ?? "")
                        + (element.size ?? "")
                        + (element.color ?? "")
                        + (order.tag ?? "");
                        console.log(str);
                      const localOrderCreate = new Date(order.created_at) ;
                      const localOrderCreateStr = localOrderCreate.getFullYear() +"-" +(localOrderCreate.getMonth()+1) + "-"+ (localOrderCreate.getDate());
                      const isInTime = searchStartDate =="" || searchEndDate == "" ||
                          (new Date(localOrderCreateStr) >= new Date(searchStartDate) &&
                          new Date(localOrderCreateStr) < new Date(searchEndDate ));
                      const hasStr = searchStr == "" || str.includes(searchStr)  ;
                      console.log(new Date(localOrderCreateStr), new Date(searchStartDate), new Date(searchEndDate), isInTime)
                      if(hasStr &&  isInTime ) 
                      {   console.log(collector);
                          collector.push(
                            <tr key={index}>
                              <td>{order.tag}</td>
                              <td>{element.serial} </td>
                              <td>{element.name}</td>
                              <td>{new Date(order.created_at).toLocaleString()}</td>
                              <td>{element.size}</td>
                              <td>{element.color}</td>
                              <td>{order.value}</td>
                              <td>

                                  <Button  size="sm" onClick = {(e) => handleSaveAmount(order.id, element.id, index, order.value)} variant="primary" id="button-addon2" disabled={order.shipment}>
                                    出貨
                                  </Button>

                              </td>
                              <td>{order.shipment_date}</td>
                            </tr>
                          )

                      }

                      return collector;
                   }, [])

                })
             }
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
      <Modal
        size="lg"
        show={articleShow}
        onHide={() => setArticleShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
        align="center"
      >  
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            確認
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            { `⏰限量`+(amount ||"")+`組，${billtime}上架 `}
          </p>
          <p>
            {`🔷【${serial} ${name}】🔷` }
          </p>
          <p>
            {`🌸說明：${sentence}`}

          </p>
          <p>
            {`🌸售價：${price} `}
          </p>
          <p>
            {`🌸尺寸/顏色：${example} `}
          </p>
          <p>
          {`📝留言範例:${remark} `}
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={()=> navigator.clipboard.writeText(      
            `⏰限量`+(amount ||"")+`組，${billtime}上架 \n` +
            `🔷【${serial} ${name}】🔷 \n` +
            `🌸說明：${sentence} \n` +
            `🌸售價：${price} \n`+
            `🌸尺寸/顏色：${example} \n📝留言範例:${remark} \n`).then(()=> setArticleShow(false))}>
            複製
          </Button>
        </Modal.Footer>
      </Modal>
  </div>
  );
}

export default CommodityRecord;

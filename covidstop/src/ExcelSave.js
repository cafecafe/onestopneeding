  import XLSX from 'xlsx';
  import {entozh} from './entozh';
  const wopts = {
    bookType: 'xlsx', // 要生成的文件類型
    bookSST: false, // 是否生成Shared String Table，官方解釋是，如果開啟生成速度會下降，但在低版本IOS設備上有更好的兼容性
    type: 'binary'
  };
  // export const handleExportAll = (nowdata) => {
  //   nowdata = handleDistinct(nowdata);
  //   console.log(nowdata);
  //   nowdata = handleData(nowdata);
  //   const sheet = covertJsonToSheetWithZh(nowdata);
  //   const date = new Date();
  //   openDownloadDialog(sheet2blob(sheet,undefined), `${date.getFullYear()}.${date.getMonth()+1}.${date.getDate()}-po文.xlsx`);
  // }
  export const exportMember = (data) => {
    const sheet = covertJsonToSheetWithZh(data);
    const date = new Date();
    openDownloadDialog(sheet2blob(sheet,undefined), `${date.getFullYear()}.${date.getMonth()+1}.${date.getDate()}-會員資料.xlsx`);
  }
  export const handleExport = (data, name ="output") => {
    const sheet = covertJsonToSheetWithZh(data);
    const date = new Date();
    openDownloadDialog(sheet2blob(sheet,undefined), `${date.getFullYear()}.${date.getMonth()+1}.${date.getDate()}-`+name+`.xlsx`);
  }
  export const exportShipmentOrder = (data) => {
    const memberOrder = data.reduce( (carry , element) => {
      const orders = element.member_order.map(order => {
        return {
          member_number: order.tag,
          commodity_serial : element.serial, 
          commodity_name : element.name,
          commodity_size : element.size,
          commodiy_color : element.color,
          amount : order.value,
          record : new Date(order.created_at).toLocaleString(),
          shipment_date: order.shipment_date,
          shipment : order.shipment
        }

      });
      return carry.concat(orders);
    }, [])
    const sheet = covertJsonToSheetWithZh(memberOrder);
    const date = new Date();
    openDownloadDialog(sheet2blob(sheet,undefined), `${date.getFullYear()}.${date.getMonth()+1}.${date.getDate()}-會員訂單.xlsx`);
  } 
  // export const handleExportTemp =(nowdata) => {
  //   console.log(nowdata);
  //   nowdata = handleOrderToStr(nowdata);
  //   const date = new Date();  
  //   const sheet = XLSX.utils.json_to_sheet(nowdata);
  //   openDownloadDialog(sheet2blob(sheet,undefined), `${date.getFullYear()}.${date.getMonth()+1}.${date.getDate()}-輸入檔.xlsx`);
  // }
  const handleOrderToStr = (data) => {
    return data.map(item => {
          console.log(item );
      item['wholesaleOrderStr'] = item.whole_sale.reduce((str, item) => 
        str + `${item.tag},${item.value} \n`
      , "")
      item['membersaleOrderStr'] = item.member_sale.reduce((str, item) => 
        str + `${item.tag},${item.value} \n`
      , "")
      item['memberOrderStr'] = item.member_order.reduce((str, item) => 
        str + `${item.tag},${item.value} \n`
      , "")
      item['manuOrderStr'] = item.manu_order.reduce((str, item) => 
        str + `${item.tag},${item.value} \n`
      , "")
      item['clientOrderStr'] = item.client_order.reduce((str, item) => 
        str + `${item.tag},${item.value} \n`
      ,"")
      return item;
  
    })
  }

  const handleGroupOrder = (data, currentDataName, orderKey) => {

    return data.reduce( (resultOrder , item) => {
      if(item.name == currentDataName) {
          item[orderKey].forEach( (order,index) => {
            resultOrder.push((order.tag || "")+" "+item.color+","+item.size+"+"+order.value);
          });
      }
      return resultOrder;
    },[]);
  }
  // const handleDistinct = (data) => {
  //   const names = data.map(i => i.name);
  //   const  distinctData = data.filter((element , index, self) => names.indexOf(element.name) === index);
  //   return distinctData.map( item =>{

  //     return Object.assign(item, {
  //       member_order : handleGroupOrder(data, item.name, "member_order"),
  //       manu_order : handleGroupOrder(data, item.name, "manu_order"),
  //       client_order : handleGroupOrder(data, item.name, "client_order")

  //     })

  //   })

  // }
  const handleData = (data) => {
    return data.map(item => {
          console.log(item);
      const description = 
      `⏰限量`+(item.amount ||"")+`組，${item.bill_time}上架 \n` +
      `🔷【${item.serial} ${item.name}】🔷 \n` +
      `🌸說明：${item.sentence} \n` +
      `🌸售價：${item.price} \n`+
      `🌸尺寸/顏色：${item.example} \n📝留言範例:${item.remark} \n`;
      const wholeSale = item.whole_sale.reduce((str, item) => 
        str + (item.tag || "")  +`${item.value} \n`
      , "")
      const memberSale = item.member_sale.reduce((str, item) => 
        str + (item.tag || "") + `${item.value} \n`
      , "")
      const memberOrder = item.member_order.reduce((str, item) => 
        str += item + "\n"
      , "")
      const manuOrder = item.manu_order.reduce((str, item) => 
        str += item + "\n"
      , "")
      const clientOrder = item.client_order.reduce((str, item) => 
        str += item + "\n"
      , "")
      return {
          description : description,
          wholeSale : wholeSale ,
          memberSale : memberSale ,
          memberOrder : memberOrder,
          clientOrder : clientOrder,
          manuOrder : manuOrder,
          saveAmount : item.saveAmount,

      }
    })
  }
  const covertJsonToSheetWithZh = (nowdata) => {
    const json = convertTitleToZh(nowdata);
    return XLSX.utils.json_to_sheet(json);
  }
  const convertTitleToZh = (nowdata, filename) => {
    console.log('covertJsonToSheetWithZh',nowdata);
      return nowdata.map((item) => {
        return Object.keys(item).reduce((newData, key) => {
          const newKey = entozh[key] || key
          if(entozh[key] != false ) newData[newKey] = item[key];
          return newData
        }, {})
    });
  }
  const openDownloadDialog = (url, saveName) => {
    if (typeof url == 'object' && url instanceof Blob) {
        url = URL.createObjectURL(url); // 創建blob地址
    }
    var aLink = document.createElement('a');
        aLink.href = url;
        aLink.download = saveName || ''; // HTML5新增的屬性，指定保存文件名，可以不要後綴，注意，file:///模式下不會生效
    var event;
    if (window.MouseEvent) event = new MouseEvent('click');
    else {
          event = document.createEvent('MouseEvents');
       event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    }
        aLink.dispatchEvent(event);
  }
  // const sheet2blob = (csv, sheetName) => {

  //   var blob = new Blob(["\uFEFF"+csv], {
  //     encoding: "UTF-8",
  //     type: "text/csv;charset=utf-8"
  //   });
  //   console.log(blob);
  //   return blob;
  // }
const sheet2blob = (sheet, sheetName) => {
  sheetName = sheetName || 'sheet1';
  var workbook = {
    SheetNames: [sheetName],
    Sheets: {}
  };
  workbook.Sheets[sheetName] = sheet; // 生成excel的配置項
  var wopts = {
    bookType: 'xlsx', // 要生成的文件類型
    bookSST: false, // 是否生成Shared String Table，官方解釋是，如果開啟生成速度會下降，但在低版本IOS設備上有更好的兼容性
    type: 'binary'
  };
  var wbout = XLSX.write(workbook, wopts);
  var blob = new Blob([s2ab(wbout)], {
    type: "application/octet-stream"
  }); // 字符串轉ArrayBuffer
  function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
  }
  return blob;
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommodityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commodities', function (Blueprint $table) {
            $table->increments("id")->autoIncrement();
            $table->text("serial")->nullable();
            $table->text("name")->nullable();
            $table->text('amount')->nullable();;
            $table->date("bill_time")->useCurrent();
            $table->text("sentence")->nullable();
            $table->text("price")->nullable();
            $table->text('size')->nullable();
            $table->text("color")->nullable();
            $table->text("style")->nullable();
            $table->text("example")->nullable();
            $table->text("remark")->nullable(); 
            $table->text('saveAmount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commodities', function (Blueprint $table) {
            //  
        });
        Schema::dropIfExists('commodities');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->primary('number');
            $table->timestamps();
            $table->string("number");
            $table->string("member_class")->nullable();
            $table->string("fb_name")->nullable();
            $table->string("l_name")->nullable();
            $table->string("name")->nullable();
            $table->string("phone")->nullable();
            $table->string("address")->nullable();
            $table->string("destination")->nullable();
            $table->string("birthday",27)->nullable();
            $table->string("email")->nullable();
            $table->string("method")->nullable();
            $table->integer("shop_gold")->nullable()->default(0);
            $table->string("remark")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commodity extends Model
{
    //use HasFactory;
    protected $guarded = [];
    public function manu_order()
    {
        return $this->hasMany(ManuOrder::class);
    }
    public function client_order()
    {
        return $this->hasMany(ClientOrder::class);
    }
    public function member_order()
    {
        return $this->hasMany(MemberOrder::class);
    }
    public function member_sale()
    {
        return $this->hasMany(MemberSale::class);
    }
    public function whole_sale()
    {
        return $this->hasMany(WholeSale::class);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManuOrder extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;
    public function commondity()
    {
        return $this->belongsTo(Commondity::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTime;
class MemberOrder extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function commondity()
    {
        return $this->belongsTo(Commondity::class);
    }
    public function member()
    {
        return $this->belongsTo(Member::class, "number", "tag");
    }
}

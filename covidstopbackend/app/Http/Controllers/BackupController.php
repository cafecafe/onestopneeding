<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\DB;

class BackupController extends Controller
{
    static function sendEmail() {
        Mail::to(env("email_target"))->send(new OrderShipped());
        return response()->json(["status" => 'OK']);

    }
    public function importBackupSql(Request $request){
        $sql_dump = $request->only(["sql"]);
        DB::connection()->getPdo()->exec($sql_dump);
    }
}

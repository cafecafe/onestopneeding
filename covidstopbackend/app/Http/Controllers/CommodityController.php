<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Commodity;
use App\Models\MemberOrder;
use Carbon\Carbon;

class CommodityController extends Controller
{
    public static $mainKey = [
            "serial",
            "name",
            'amount',
            "bill_time",
            "sentence",
            "price ",
            'size',
            "color",
            "style",
            "example",
            "remark",
            'saveAmount'
    ];
    public static $orderKey = [
            "member_order",
            "client_order",
            "manu_order",
            "member_sale",
            "whole_sale"
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json( Commodity::with(['member_order', 'manu_order','client_order','whole_sale','member_sale'])->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            "serial" => "P062100803",
            "name" => " 菜瓜布按壓洗碗精盒",
            "amount" => 100,
            "bill_time" => date("m/d H:s"),
            "sentence" => "描述點什麼",
            "price" => null,
            "size" => "L",
            "color" => "紅、黑 ....",
            "style" => "A、B、C、D、E、F ...",
            "example" => "A+1、B+1、C+1 ...",
            "remark" => "款式要備註，系統才能抓單哦",
            "merchant_url" => "https://....",
            "member_order" => [
                0 =>[
                    'member' => "A1",
                    'tag' => "藍",
                    'amount' => 2,
                    'remark' => null
                ]

            ],
            'member_order' => [
                0 =>[
                    'date' => date("m/d"),
                    'tag' => "藍",
                    'amount' => 2,
                    'remark' => null
                ]
            ],
            'member_order' => [
                0 =>[
                    'tag' => "藍",
                    'amount' => 2,
                    'remark' => null
                ]
            ],
            'sale' => [
                0 => [
                    'tag' => " ..",
                    'type' => 0,
                    'price' => 100,
                    'remark' => "type 是int ，0是批發1是團購 "
                ],
                1 => [
                    'tag' => " ..",
                    'type' => 1,
                    'price' => 200,
                    'remark' => null
                ]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $main_data = $request->only(self::$mainKey);
        $main_data["bill_time"] = date_create($main_data["bill_time"]);
        // $main_data["bill_time"] = $bill_date->format("Y-m-d H:i:s");
        $commodity = Commodity::updateOrCreate(["serial" => $main_data["serial"], "color" => $main_data["color"], "size" => $main_data["size"] ], $main_data);
        $order_data = $request->only(self::$orderKey);
        $orderElement =[];
        $orderElement["member_order"] = collect([]);
        foreach ($order_data["member_order"] as $key => $value) {
            $orderElement["member_order"]->push($value["id"]);
        }

        $orderElement["client_order"] = collect([]);
        foreach ($order_data["client_order"] as $key => $value) {

            $orderElement["client_order"]->push($value["id"]);
        }

        $orderElement["manu_order"] = collect([]);
        foreach ($order_data["manu_order"] as $key => $value) {

            $orderElement["manu_order"]->push($value["id"]);
        }

        $orderElement["member_sale"] = collect([]);
        foreach ($order_data["member_sale"] as $key => $value) {

            $orderElement["member_sale"]->push($value["id"]);
        }

        $orderElement["whole_sale"] = collect([]);
        foreach ($order_data["whole_sale"] as $key => $value) {

            $orderElement["whole_sale"]->push($value["id"]);
        }

        $commodity->member_order()->whereNotIn("id", $orderElement["member_order"])->delete();
        $commodity->client_order()->whereNotIn("id", $orderElement["client_order"])->delete();
        $commodity->manu_order()->whereNotIn("id", $orderElement["manu_order"])->delete();
        $commodity->whole_sale()->whereNotIn("id", $orderElement["whole_sale"])->delete();
        $commodity->member_sale()->whereNotIn("id", $orderElement["member_sale"])->delete();


        foreach ($order_data["member_order"] as $key => $value) {
            if($value["id"] != null) $commodity->member_order()->updateOrCreate(["commodity_id" => $commodity->id], ["value" => $value["value"], "tag" => $value["tag"]]);
            else $commodity->member_order()->create($value);
        }

        foreach ($order_data["client_order"] as $key => $value) {
            if($value["id"] != null) $commodity->client_order()->updateOrCreate(["commodity_id" => $commodity->id], ["value" => $value["value"], "tag" => $value["tag"]]);
            else $commodity->client_order()->create($value);
        }

        foreach ($order_data["manu_order"] as $key => $value) {
            if($value["id"] != null) $commodity->manu_order()->updateOrCreate(["commodity_id" => $commodity->id], ["value" => $value["value"], "tag" => $value["tag"]]);
            else $commodity->manu_order()->create($value);
        }

        foreach ($order_data["member_sale"] as $key => $value) {
            if($value["id"] != null) $commodity->member_sale()->updateOrCreate(["commodity_id" => $commodity->id], ["value" => $value["value"], "tag" => $value["tag"]]);
            else $commodity->member_sale()->create($value);

        }

        foreach ($order_data["whole_sale"] as $key => $value) {
            if($value["id"] != null) $commodity->whole_sale()->updateOrCreate(["commodity_id" => $commodity->id], ["value" => $value["value"], "tag" => $value["tag"]]);
            else $commodity->whole_sale()->create($value);
        }
        return response()->json([ "description" => "OK", 
            "data" => Commodity::with(['member_order', 'manu_order','client_order','whole_sale','member_sale'])->find($commodity->id), 
            "updated" => $commodity->updated_at > $commodity->created_at
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        $commodity = Commodity::with("member_order")->where("id", $id)->first();
        $num_str  = (int)$commodity->saveAmount;
        $variation = $request->input("variation");
        $result = $num_str - $variation ;
        if($result< 0) {
            return response()->json(["description" => "庫存不足"]);
        }
        $member_id = $request->input("member_id");
        $commodity->update(["saveAmount" => $result ]);
        $commodity->member_order()->find($member_id)->update(["shipment" => true]);
        return response()->json(['description' => "OK",  'data' => Commodity::with("member_order")->find($id) ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commodity = Commodity::find($id);
        $commodity->manu_order()->delete();
        $commodity->member_order()->delete();
        $commodity->client_order()->delete();
        $commodity->member_sale()->delete();
        $commodity->whole_sale()->delete();
        Commodity::destroy($id);
        return response()->json(["description" => "OK"]);
    }
    public function import(Request $request) {
        $import_data = $request->input("import");
        $len = count($import_data);
        for ($i=0; $i < $len ; $i++) { 

            $main_data =  collect($import_data[$i]["main"]);
            $main_data["bill_time"] = date_create($main_data["bill_time"]);
            if(!$main_data["bill_time"]) $main_data["bill_time"] = date_create();
             
            $order_data = $import_data[$i]["order"];
            $orderElement["member_order"] = collect([]);
            $commodity = Commodity::updateOrCreate(["serial" => $main_data["serial"], "color" => $main_data["color"], "size" => $main_data["size"] ], $main_data->toArray());
            foreach ($order_data["member_order"] as $key => $value) {
                $orderElement["member_order"]->push($value["id"]);
            }

            $orderElement["client_order"] = collect([]);
            foreach ($order_data["client_order"] as $key => $value) {

                $orderElement["client_order"]->push($value["id"]);
            }

            $orderElement["manu_order"] = collect([]);
            foreach ($order_data["manu_order"] as $key => $value) {

                $orderElement["manu_order"]->push($value["id"]);
            }

            $orderElement["member_sale"] = collect([]);
            foreach ($order_data["member_sale"] as $key => $value) {
                $orderElement["member_sale"]->push($value["id"]);
            }

            $orderElement["whole_sale"] = collect([]);
            foreach ($order_data["whole_sale"] as $key => $value) {
                $orderElement["whole_sale"]->push($value["id"]);
            } 
            $commodity->member_order()->whereNotIn("id", $orderElement["member_order"])->delete();
            $commodity->client_order()->whereNotIn("id", $orderElement["member_order"])->delete();
            $commodity->manu_order()->whereNotIn("id", $orderElement["member_order"])->delete();
            $commodity->whole_sale()->whereNotIn("id", $orderElement["whole_sale"])->delete();
            $commodity->member_sale()->whereNotIn("id", $orderElement["member_sale"])->delete();

            foreach ($order_data["member_order"] as $key => $value) {
                $commodity->member_order()->updateOrCreate(["id" => $value["id"] ], ["value"=> $value["value"]]);
            }

            foreach ($order_data["client_order"] as $key => $value) {
                $commodity->client_order()->updateOrCreate(["id" => $value["id"] ], ["value"=> $value["value"]]);
            }

            foreach ($order_data["manu_order"] as $key => $value) {
                $commodity->manu_order()->updateOrCreate(["id" => $value["id"] ], ["value"=> $value["value"]]);
            }

            foreach ($order_data["member_sale"] as $key => $value) {
                $commodity->member_sale()->updateOrCreate(["id" => $value["id"] ], ["value"=> $value["value"]]);

            }

            foreach ($order_data["whole_sale"] as $key => $value) {
                $commodity->whole_sale()->updateOrCreate(["id" => $value["id"] ], ["value"=> $value["value"]]);
            }     
        }
        return response()->json([ "description" => "OK"]);
    }


}

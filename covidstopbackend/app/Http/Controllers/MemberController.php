<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return response()->json(Member::all()); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            "name",
            "number",
            "member_class",
            "fb_name",
            "l_name",
            "address",
            "destination",
            "birthday",
            "method",
            "remark",
            "shop_gold",
            "phone",
            "email"
        ]);
        $member = Member::updateOrCreate([ "number" => $data["number"]] ,$data);

        return response()->json([
            "description" => "OK",
            "data" => $member,
            "updated" => $member->updated_at > $member->created_at
    ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Member::where("number", $id)->delete();
        return response()->json(["description" => "OK"]);
    }
}

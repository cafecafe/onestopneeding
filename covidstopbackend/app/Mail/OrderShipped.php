<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Commodity;
use Carbon\Carbon;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct()
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $header = "bill_time,amount,serial,name,sentence,price,size,color,example,remark,saveAmount,wholesaleOrderStr,membersaleOrderStr,memberOrderStr,manuOrderStr,clientOrderStr,";
        // $output = Commodity::get()->reduce(function($carry ,$item){
        //     $carry .= $item->bill_time . ',';
        //     $carry .= $item->amount . ',';
        //     $carry .= $item->serial . ',';
        //     $carry .= $item->name . ',';
        //     $carry .= $item->sentence . ',';
        //     $carry .= $item->price . ',';
        //     $carry .= $item->size . ',';
        //     $carry .= $item->color . ',';
        //     $carry .= $item->example . ',';
        //     $carry .= $item->remark . ',';
        //     $carry .= $item->saveAmount . ',';
        //     $carry .= $this->handleArrayToStr( $item->whole_sale()->get());
        //     $carry .= $this->handleArrayToStr( $item->member_sale()->get());
        //     $carry .= $this->handleArrayToStr( $item->member_order()->get());
        //     $carry .= $this->handleArrayToStr( $item->manu_order()->get());
        //     $carry .= $this->handleArrayToStr( $item->member_sale()->get());
        //     return $carry;
        // }, $header);
        return $this
            ->markdown('emails.orders.shipped',['url' => "123"])
            ->attachFromStorage('/backup/'."backup-" . Carbon::now()->format('Y-m-d') . ".gz");
    }

    public function handleArrayToStr($collection) {
        return $collection->reduce(function($carry,$item) {
            return $carry .= $item->tag . "、" . $item->value .',';

        },"");

    }
}

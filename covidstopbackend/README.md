## 環境建置
1.安裝套件
```sh
    composer install
```
2.複製.env.example

3.設定env，DB_DATABASE設為 covid_stop

4.生成鑰匙

```sh
    php artisan key:generate
```
5.建立資料庫 covid_stop，資料庫系統為mysql

6. 生成表格

```sh
    php artisan migrate
```